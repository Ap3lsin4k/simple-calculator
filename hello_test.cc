#include <gtest/gtest.h>
#include "calculator.h"

// Demonstrate some basic assertions.
TEST(HelloTest, BasicAssertions) {
  // Expect two strings not to be equal.
  EXPECT_STRNE("hello", "world");
  // Expect equality.
  Calculator calc = Calculator();
  EXPECT_EQ(calc.Add(4.8, 4.8), 10);
}

